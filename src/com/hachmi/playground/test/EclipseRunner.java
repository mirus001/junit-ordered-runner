package com.hachmi.playground.test;

import org.junit.runner.Description;
import org.junit.runner.Runner;
import org.junit.runner.notification.RunNotifier;
import org.junit.runners.BlockJUnit4ClassRunner;
import org.junit.runners.model.InitializationError;

public class EclipseRunner extends BlockJUnit4ClassRunner{
	private final Class<?> testClass;
	
	public EclipseRunner(Class<?> testClass) throws InitializationError{
		super(testClass);
		this.testClass = testClass;
		System.out.println("Eclipse Runner called");
	}
	
	public Description getDescription(){
		return super.getDescription();
    }
	
	public void run(RunNotifier notifier){
		super.run(notifier);
    }
}
