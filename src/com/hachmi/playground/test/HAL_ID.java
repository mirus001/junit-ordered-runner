package com.hachmi.playground.test;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface HAL_ID {
 
    public String tag() default "";
}