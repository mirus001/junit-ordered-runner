package com.hachmi.playground.test;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import org.junit.runner.Description;
import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.Runner;
import org.junit.runner.notification.Failure;
import org.junit.runner.notification.RunNotifier;
import org.junit.runners.JUnit4;

import static org.junit.Assert.fail;

public class JUnitRunManager extends Runner {
	protected Class<?> testClass;
    protected Runner delegateRunner;

	public JUnitRunManager(Class<?> testClass) {
        super();
        this.testClass = testClass;
        Class<? extends Runner> delegateClass = null;
        
        if ( System.getProperty( "sun.java.command" ).contains( "org.eclipse.jdt" ) ) {
            System.out.println("called from eclipse");
            //delegateClass = EclipseRunner.class;
            delegateClass = JUnit4.class;
        }
        else {
            System.out.println("NOT called from eclipse");
        }
        
        try {
            Constructor<? extends Runner> constructor = delegateClass.getConstructor( Class.class );
            delegateRunner = constructor.newInstance(testClass);
        } catch (NoSuchMethodException e) {
            fail( delegateClass.getName() + " must contain a public constructor with a " + Class.class.getName() + " argument.");
        } catch (SecurityException e) {
            throw new RuntimeException("SecurityException during instantiation of " + delegateClass.getName() );
        } catch (InstantiationException e) {
            throw new RuntimeException("Error while creating " + delegateClass.getName() );
        } catch (IllegalAccessException e) {
            throw new RuntimeException("Error while creating " + delegateClass.getName() );
        } catch (IllegalArgumentException e) {
            throw new RuntimeException("Error while creating " + delegateClass.getName() );
        } catch (InvocationTargetException e) {
            throw new RuntimeException("Error while creating " + delegateClass.getName() );
        }
    }
	
	private JUNIT_RUNNER findAnnotationInClassHierarchy(Class<?> testClass) {
		JUNIT_RUNNER annotation = testClass.getAnnotation(JUNIT_RUNNER.class);
        if (annotation != null) {
            return annotation;
        }

        Class<?> superClass = testClass.getSuperclass();
        if (superClass != null) {
            return findAnnotationInClassHierarchy(superClass);
        }

        return null;
    }
	
	@Override
	public Description getDescription() {
		// TODO Auto-generated method stub
		return ((Runner)delegateRunner).getDescription();
	}

	@Override
	public void run(RunNotifier arg0) {
		System.out.println("run called from JUnitRunManager");
	}

}
