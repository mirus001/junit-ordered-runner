package com.hachmi.playground.test.custom;

import org.junit.FixMethodOrder;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

import com.hachmi.playground.test.EclipseRunner;
import com.hachmi.playground.test.JUNIT_RUNNER;
import com.hachmi.playground.test.JUnitRunManager;

@FixMethodOrder(MethodSorters.TestSortTag)
@JUNIT_RUNNER( eclipse=EclipseRunner.class )
@RunWith( JUnitRunManager.class)
public class GenesisTest {

}
