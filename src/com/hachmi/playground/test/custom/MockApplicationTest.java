package com.hachmi.playground.test.custom;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.FixMethodOrder;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

import com.hachmi.playground.test.EclipseRunner;
import com.hachmi.playground.test.JUNIT_RUNNER;
import com.hachmi.playground.test.JUnitRunManager;
import com.hachmi.playground.test.TestSortTag;


public class MockApplicationTest{

	@BeforeClass
	public static void setup() {
		System.out.println("BEFORE CLASS");
	}
	@Before
	public void baseState() {
		System.out.println("BEFORE METHOD");
	}
	@After
	public void cleanup() {
		System.out.println("AFTER METHOD");
	}

	
	@TestSortTag(tag ="3")
	@Test
	public void test_C(){
		System.out.println("TEST_C");
		fail("test003");
	}
	
	@TestSortTag(tag ="1")
	@Test
	public void test_A() {
		System.out.println("TEST_A");
		fail("test001");
	}
	
	@TestSortTag(tag ="7")
	@Test
	public void test_D(){
		System.out.println("TEST_D");
		fail("test004");
	}

	@TestSortTag(tag ="2")
	@Test
	public void test_B(){
		System.out.println("TEST_B");
		fail("test003");
	}
	
	@TestSortTag(tag ="6")
	@Test
	public void test_E() {
		System.out.println("TEST_E");
		fail("test005");
	}
	
	@TestSortTag(tag ="4")
	@Test
	public void test_G(){
		System.out.println("TEST_G");
		fail("test007");
	}
	
	@TestSortTag(tag ="5")
	@Test
	public void test_F(){
		System.out.println("TEST_F");
		fail("test006");
	}
}
